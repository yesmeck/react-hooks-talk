# React Hooks

---

- Mixins
- HOC
- Render Props

---

## Mixins 的问题

- ES6 class 不支持
- 不够直接
- 命名冲突
- 容易雪崩

---

## Mixins 的问题 - 隐式依赖

mixin 依赖组件

组件依赖 mixin

mixin 有又可以依赖组件

---

## Mixins 的问题 - 命名冲突

FluxListenerMixin defines handleChange()
WindowSizeMixin defines handleChange(),

---

## Mixins 的问题 - 容易雪崩

- HoverMixin
  - handleMouseEnter()
  - handleMouseLeave()
  - isHovering()
  - getHoverOptions()
- TooltipMixin
  - isHovering()
  - componentDidUpdate()
  - getTooltipOptions()o

---

## HOC

- 命名冲突
- 隐式依赖

---

## HOC - 命名冲突

---

## HOC - 不够直接

---

## Render Props

- Wrapper Hell
- 在 render 外使用不方便

---

## Render Props - Wrapper Hell

---

## Render Props - 使用不便

---

## 💥 React Hooks

---

## API
